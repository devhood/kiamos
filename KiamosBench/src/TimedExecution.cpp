/*******************************************************************************
 * KiamosBench for benchmarking the cpu
 * Copyright (C) 2016 Ioannis Panagiotopoulos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/*
 * TimedExecution.cpp
 *
 *  Created on: 11 Μαρ 2016
 *      Author: klapeto
 */

#include "TimedExecution.hpp"
#include "Operation.hpp"
#include <thread>

namespace KiamosBench {

void TimedExecution::start() {
	if (operation != nullptr) {
		std::chrono::time_point<std::chrono::high_resolution_clock> startT,
				endT;
		startT = std::chrono::high_resolution_clock::now();
		operation->executeOperation();
		endT = std::chrono::high_resolution_clock::now();

		elapsedTime = endT - startT;
	}
}

} /* namespace KiamosBench */


