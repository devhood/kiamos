/*
 * Operation.hpp
 *
 *  Created on: 11 Μαρ 2016
 *      Author: klapeto
 */

#ifndef OPERATION_HPP_
#define OPERATION_HPP_

namespace KiamosBench {

class Operation {
public:

	virtual void executeOperation()=0;

	Operation() {

	}
	virtual ~Operation() {

	}
};

} /* namespace KiamosBench */

#endif /* OPERATION_HPP_ */
