/*
 * IntegerOperations.cpp
 *
 *  Created on: 12 Μαρ 2016
 *      Author: klapeto
 */

#include "IntegerOperations.hpp"

#include <thread>

namespace KiamosBench {

void IntegerOperations::executeOperation() {
	operations = 0;

	if (multithreaded) {
		int th_num = std::thread::hardware_concurrency();

		std::thread thread[th_num];

		for (int i = 0; i < th_num; ++i) {
			thread[i] = std::thread(&IntegerOperations::process, this);
		}

		for (int i = 0; i < th_num; ++i) {
			thread[i].join();
		}
	} else {
		process();
	}

}

void IntegerOperations::process() {

	long int x = reinterpret_cast<long int>(this) + 1, y =
			reinterpret_cast<long int>(this) - 6, z =
			reinterpret_cast<long int>(this) + 2;

	unsigned long int c = 0;


	for (unsigned long int i = 1; i < 2000000000; ++i) {
#if INSTRUCTION_DEPENDENCY
		x += y + z - x;
		z -= x * y * z;
		y /= (z / x) / y;
		c += 9;
#else
		x += x;
		y += y;
		z /= z;
		c += 3;
#endif
	}

	{
		{
			std::unique_lock<std::mutex> lock;
			result += x + (y / z);
			operations += c + 5;
		}
	}
}

} /* namespace KiamosBench */


