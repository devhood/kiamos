/*
 * FloatOperations.hpp
 *
 *  Created on: 12 Μαρ 2016
 *      Author: klapeto
 */

#ifndef OPERATIONS_FLOATOPERATIONS_HPP_
#define OPERATIONS_FLOATOPERATIONS_HPP_

#include "Operation.hpp"
#include <mutex>

namespace KiamosBench {

class FloatOperations: public Operation {
public:

	unsigned long int getOperationPerformed() const {
		return operations;
	}

	bool isMultithreaded() const {
		return multithreaded;
	}

	void setMultithreaded(bool multithreaded) {
		this->multithreaded = multithreaded;
	}

	float getResult() const {
		return result;
	}

	void executeOperation();

	FloatOperations();
	~FloatOperations();
private:
	std::mutex mutex;
	unsigned long int operations;
	float result;

	bool multithreaded;

	void process();
};

} /* namespace KiamosBench */

#endif /* OPERATIONS_FLOATOPERATIONS_HPP_ */
