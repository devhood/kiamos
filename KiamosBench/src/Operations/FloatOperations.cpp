/*
 * FloatOperations.cpp
 *
 *  Created on: 12 Μαρ 2016
 *      Author: klapeto
 */

#include "FloatOperations.hpp"

#include <thread>

namespace KiamosBench {

FloatOperations::FloatOperations() :
		operations(0), result(0.0f), multithreaded(false) {

}

FloatOperations::~FloatOperations() {

}

void FloatOperations::executeOperation() {

	operations = 0;
	if (multithreaded) {
		int th_num = std::thread::hardware_concurrency();

		std::thread thread[th_num];

		for (int i = 0; i < th_num; ++i) {
			thread[i] = std::thread(&FloatOperations::process, this);
		}

		for (int i = 0; i < th_num; ++i) {
			thread[i].join();
		}
	} else {
		process();
	}

}

void FloatOperations::process() {

	float x = reinterpret_cast<long int>(this) / 5.6584f, y =
			reinterpret_cast<long int>(this) / 62.8646f, z =
			reinterpret_cast<long int>(this) / 0.02546f;

	unsigned long int c = 0;

	for (unsigned long int i = 1; i < 2000000000; ++i) {
#if INSTRUCTION_DEPENDENCY
		x += y + z - x;
		z -= x * y * z;
		y /= (z / x) / y;
		c += 9;
#else
		x += x;
		y += y;
		z /= z;
		c += 3;
#endif

	}

	{
		std::unique_lock<std::mutex> lock;
		result += x + (y / z);
		operations += c + 5;
	}
}

} /* namespace KiamosBench */
