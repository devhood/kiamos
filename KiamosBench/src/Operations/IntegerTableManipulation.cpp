/*
 * IntegerTableManipulation.cpp
 *
 *  Created on: 12 Μαρ 2016
 *      Author: klapeto
 */

#include "IntegerTableManipulation.hpp"

#include <thread>

namespace KiamosBench {

IntegerTableManipulation::IntegerTableManipulation() :
		size(10000), multithreaded(false) {
	data = new int[10000];
}

IntegerTableManipulation::IntegerTableManipulation(size_t size) :
		size(size), multithreaded(false) {
	data = new int[size];
}

IntegerTableManipulation::~IntegerTableManipulation() {
	delete data;
}

void IntegerTableManipulation::executeOperation() {
	if (multithreaded) {
		int th_num = std::thread::hardware_concurrency();

		size_t chunk_per_thread = size / th_num;

		std::thread thread[th_num];

		for (int i = 0; i < th_num; ++i) {
			thread[i] = std::thread(&IntegerTableManipulation::processChunk,
					this, i * chunk_per_thread,
					((i + 1) * chunk_per_thread) - 1);
		}

		for (int i = 0; i < th_num; ++i) {
			thread[i].join();
		}
	} else {
		for (size_t i = 0; i < size; ++i) {
			data[i] = size - (i * 2);
		}
	}
}

void IntegerTableManipulation::processChunk(size_t start, size_t end) {
	for (size_t i = start; i < end; ++i) {
		data[i] = size - (i * 2);
	}
}

} /* namespace KiamosBench */
