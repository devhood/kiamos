/*
 * IntegerOperations.hpp
 *
 *  Created on: 12 Μαρ 2016
 *      Author: klapeto
 */

#ifndef OPERATIONS_INTEGEROPERATIONS_HPP_
#define OPERATIONS_INTEGEROPERATIONS_HPP_

#include "Operation.hpp"
#include <mutex>

namespace KiamosBench {

class IntegerOperations: public Operation {
public:

	bool isMultithreaded() const {
		return multithreaded;
	}

	void setMultithreaded(bool multithreaded) {
		this->multithreaded = multithreaded;
	}

	int getResult() const {
		return result;
	}

	void executeOperation();

	unsigned long int getOperationPerformed() const {
		return operations;
	}

	IntegerOperations() :
			operations(0), result(0), multithreaded(false) {

	}
	~IntegerOperations() {

	}

private:
	std::mutex mutex;

	unsigned long int operations;
	int result;

	bool multithreaded;

	void process();

};

} /* namespace KiamosBench */

#endif /* OPERATIONS_INTEGEROPERATIONS_HPP_ */
