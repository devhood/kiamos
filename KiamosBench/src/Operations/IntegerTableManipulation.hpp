/*
 * IntegerTableManipulation.hpp
 *
 *  Created on: 12 Μαρ 2016
 *      Author: klapeto
 */

#ifndef OPERATIONS_INTEGERTABLEMANIPULATION_HPP_
#define OPERATIONS_INTEGERTABLEMANIPULATION_HPP_

#include "Operation.hpp"
#include <stddef.h>

namespace KiamosBench {

class IntegerTableManipulation: public Operation {
public:

	bool isMultithreaded() const {
		return multithreaded;
	}

	void setMultithreaded(bool multithreaded) {
		this->multithreaded = multithreaded;
	}

	size_t getSize() const {
		return size;
	}

	void executeOperation();

	IntegerTableManipulation();
	IntegerTableManipulation(size_t size);
	~IntegerTableManipulation();
private:
	int *data;
	size_t size;

	bool multithreaded;

	void processChunk(size_t start, size_t end);
};

} /* namespace KiamosBench */

#endif /* OPERATIONS_INTEGERTABLEMANIPULATION_HPP_ */
