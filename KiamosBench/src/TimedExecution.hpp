/*
 * TimedExecution.hpp
 *
 *  Created on: 11 Μαρ 2016
 *      Author: klapeto
 */

#ifndef TIMEDEXECUTION_HPP_
#define TIMEDEXECUTION_HPP_

#include <chrono>

namespace KiamosBench {

class Operation;

class TimedExecution {
public:

	std::chrono::duration<double> getElapsedTime() {
		return elapsedTime;
	}

	void setOperation(Operation* operation) {
		this->operation = operation;
	}

	void start();

	TimedExecution() :
			operation(nullptr), elapsedTime(0) {

	}
	~TimedExecution() {

	}
private:
	Operation* operation;
	std::chrono::duration<double> elapsedTime;
};

} /* namespace KiamosBench */

#endif /* TIMEDEXECUTION_HPP_ */
