/*******************************************************************************
 * KiamosBench for benchmarking the cpu
 * Copyright (C) 2016 Ioannis Panagiotopoulos
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

#include <iostream>

#include <TimedExecution.hpp>
#include "Operations/IntegerOperations.hpp"
#include "Operations/IntegerTableManipulation.hpp"
#include "Operations/FloatOperations.hpp"

using namespace KiamosBench;

int main(void) {

	TimedExecution timed;

	IntegerOperations op;
	op.setMultithreaded(true);

	timed.setOperation(&op);

	timed.start();

	std::cout << "Operation took: " << timed.getElapsedTime().count()
			<< " seconds for \n\t" << op.getOperationPerformed() << " Operations"
			<< std::endl;

	std::cout << "\tResulting : "
			<< op.getOperationPerformed() / timed.getElapsedTime().count()
			<< " Float Operations/s" << std::endl;


	return 0;
}
